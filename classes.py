#!/usr/bin/python
# -*- coding: utf-8 -*-

import sqlite3
import urllib2
import urllib
#a ultima versao quebra no site do senado. usando 3.0.8
import BeautifulSoup
import re
import datetime
import web
import sqlite3

class Comission:
    def __init__(self, comission_fullname, casa):
        self.full_name = comission_fullname
        self.casa = casa

    def getMembers(self):
        if self.casa == 'SENADO':
            conn = sqlite3.connect('data/senadoreslist.sqlite')
            conn.row_factory = sqlite3.Row    
            c = conn.cursor()
            c.execute('SELECT * FROM comissions JOIN senadores_list ON comissions.senator_id = senadores_list.id WHERE full_name=?', [self.full_name])
            self.members = c.fetchall()
            c.close()
            return self.members
        elif self.casa == 'CAMARA':
            self.members = ''
            return self.members        

class Project:
    def __init__(self, project_id, casa):        
        self.project_id = project_id
        self.casa = casa

    def getTramitacaoSenado(self):
        tramitacao_completa = []
        self.url = 'http://www.senado.gov.br/atividade/materia/detalhes.asp?p_cod_mate=' + str(self.project_id) + '&p_sort=DESC&p_sort2=D&cmd=sort'
        html = urllib.urlopen(self.url).read().decode('iso-8859-1')
        self.html = html
        soup = BeautifulSoup.BeautifulSoup(self.html)

        # usa o BeautifulSoup para pegar os dados do projeto
        self.project_name = soup.find("div", "titulocaixa_abas" ).text
        self.project_author = soup.find("td", text="Autor: ").next.next.text
        self.project_abstract = soup.find("td", text="Ementa: ").next.next.text

        # usar o BeautifulSoup para pegar a tramitacao do projeto
        tramitacao = soup.find("div", { "id" : "DIV_TRAMITACAO" }) 
        tramite = tramitacao.findAll("div", { "style" : "clear:left; width:100%; padding-top:10px; display:block;"})
        for div in tramite:
            data = {}
            data['id'] = div.find("div", { "style" : "clear:both; width:100%;"})['id']
            data['date'] = div.find("div", "label").text
            data['where'] = div.find("div", { "style" : "float:left; width:62%; padding-left:10px;" }).text
        #POG barato porque nao consegui verificar acentos.
            if (div.find("div",text=re.compile("Situa*."))) :
                data['status'] = div.find("div", { "style" : "float:left; width:81%; padding-left:10px;" }).text
            else :
                data['status'] = "-"
            data['action'] = div.find("span", "textoAcao").text

        #   salva links para diario e textos
            links = re.findall(r'<a(.+?)href="(.+?)"(.+?)>', str(div))
            for link in links:
                full_link = "http://www.senado.gov.br/atividade/materia/%s" % link[1]
                if link[1].startswith('getTexto'):
                    data['link_texto'] = full_link
                elif link[1].startswith('verDiario'):
                    data['link_diario'] = full_link         
            tramitacao_completa.append(data)        
        self.tramitacao = tramitacao_completa
        return tramitacao_completa

    def getTramitacaoCamaraXML(self):
        tramitacao_completa=[]
        self.url = 'http://www.camara.gov.br/CamaraWS/Orgaos.asmx/ObterAndamento?sigla=PL&numero=60&ano=2003&dataIni=01/12/2003'
        xml = urllib2.urlopen(self.url).read()


    def getTramitacaoCamara(self):
        tramitacao_completa = []
        self.url = 'http://www.camara.gov.br/sileg/Prop_Detalhe.asp?id=' + str(self.project_id)
        html = urllib.urlopen(self.url).read()
        self.html = html.decode('utf-8', 'ignore')
        soup = BeautifulSoup.BeautifulSoup(self.html)

        # usa o BeautifulSoup para pegar os dados do projeto
        self.project_name = soup.find("span", text=re.compile("Proposi.*: ") ).next.text
        self.project_author = soup.find("span", text="Autor:").next
        self.project_abstract = soup.find("span", text="Ementa: ").next

        # usar o BeautifulSoup para pegar a tramitacao do projeto
        tramitacao = soup.find("table", { "class" : "tabela-1" }) 
        tramite = tramitacao.tbody.findAll("tr")
        tramite_id = 0
        for row in tramite:
            data = {}
            tramite_id = tramite_id + 1
            data['id'] = tramite_id
            data['date'] = row.td.text
            data['where'] = row.strong.next.strip('nbsp;')
            data['status'] = row.br.next.strip()
            data['action'] = row.br.next.strip()
            tramitacao_completa.append(data)
        # TODO salvar links para diario e textos

        #reverte para ordem reversa - mais novo primeiro
        tramitacao_completa.reverse()       
        self.tramitacao = tramitacao_completa
        return tramitacao_completa

class Dashboard:
    def __init__(self, projeto):
        comission = Comission(projeto.tramitacao[0]['where'], projeto.casa)
        comission.getMembers()
        milestone = datetime.datetime.strptime(projeto.tramitacao[0]['date'],'%d/%m/%Y')
        timeago = datetime.datetime.today()-milestone
  
        self.projeto = projeto
        self.project_id = projeto.project_id
        self.project_name = projeto.project_name
        self.project_abstract = projeto.project_abstract
        self.last_status = projeto.tramitacao[0]['status']
        self.where = projeto.tramitacao[0]['where']
        self.comission = comission.members
        self.date = projeto.tramitacao[0]['date']
        self.timeago = timeago.days
        if self.last_status == 'Processo Arquivado.':
            self.timeago = -1

    def render(self, msg_form):
        r = web.template.render('templates')
        html = unicode(r.dashboard(self, msg_form))
        return unicode(html)

    def renderWidget(self):
        r = web.template.render('templates')
        html = unicode(r.widget(self))
        return unicode(html)

    def writefile(self, html):
        f = open('static/'+str(self.project_id)+'.html', 'w')
        f.write(html.encode('utf-8'))
        f.close()

    def form(self, message):
        msg = web.form.Form(
            web.form.Textbox('nome'),
            web.form.Textarea('msg'),
        )
        return msg

class History():
    def __init__(self, projeto):
        self.projeto = projeto    
        self.project_id = projeto.project_id
        self.project_name = projeto.project_name
        self.project_abstract = projeto.project_abstract
        self.milestones = []

        for i, t in enumerate(projeto.tramitacao):                             
            currentstone = datetime.datetime.strptime(projeto.tramitacao[i]['date'],'%d/%m/%Y')
            try:
                laststone = datetime.datetime.strptime(projeto.tramitacao[i-1]['date'],'%d/%m/%Y')
                timeago = laststone-currentstone
            except IndexError:
                timeago = datetime.datetime.today()-currentstone
            milestone = {}
            milestone['date'] = currentstone
            milestone['timeago'] = timeago.days
            milestone['status'] = projeto.tramitacao[i]['status']
            milestone['where'] = projeto.tramitacao[i]['where']
            self.milestones.append(milestone)
    
    def render(self):
        r = web.template.render('templates')
        html = unicode(r.history(self))
        return unicode(html)

