# -*- coding: utf-8 -*-
#import sys, os
#abspath = os.path.dirname(__file__)
#sys.path.append(abspath)
#os.chdir(abspath)

from config import *
from classes import *
import sys
import os
import time
import datetime
import web
import re



urls = (
    '/', 'index',
    '/widget/(.*)', 'widget',
    '/history/(.*)/([0-9]*)', 'history',
    '/dashboard/(.*)/([0-9]*)', 'dashboard'
)

app = web.application(urls, globals())

class index:
    def GET(self):
        r = web.template.render('templates')        
        return r.index(None, None, None)

    def POST(self):
        r = web.template.render('templates')        
        i = web.input()
        lei = {}
        lei['tipo'] = i['tipo']
        lei['numero'] = i['numero']
        lei['ano'] = i['ano']

        base_url = "http://www.lexml.gov.br/urn/urn:lex:br:"
        args = ''

        if lei['tipo'] == 'plc':
            args += 'camara.deputados:projeto.lei;pl:'+ str(lei['ano']) + ';' + str(lei['numero'])
        elif lei['tipo'] == 'pls':
            args += 'senado.federal:projeto.lei;pls:'+ str(lei['ano']) + ';' + str(lei['numero'])
        elif lei['tipo'] == 'pdc':
            args += 'camara.deputados:projeto.decreto.legislativo;pdc:'+ str(lei['ano']) + ';' + str(lei['numero'])
        elif lei['tipo'] == 'plp':
            args += 'camara.deputados:projeto.lei.complementar;plp:'+ str(lei['ano']) + ';' + str(lei['numero'])
        elif lei['tipo'] == 'pec':
            args += 'senado.federal:proposta.emenda.constitucional;pec:'+ str(lei['ano']) + ';' + str(lei['numero'])
        else:
            return 'Categoria Invalida.'
        
        lei['lexml'] = base_url+args
        html = urllib.urlopen(base_url+args)          
        soup = BeautifulSoup.BeautifulSoup(html)

        camara = soup.find('a', { 'href' : re.compile('www.camara.gov.br/sileg/Prop_Detalhe.asp\?id=[0-9]*') })

        senado = soup.find('a', { 'href' : re.compile('www.senado.gov.br/sf/atividade/Materia/detalhes.asp\?p_cod_mate=[0-9]*') })

        if camara:
            camara_link = re.search('id=([0-9]*)', camara['href']).group(1)
        else:
            camara_link = None

        if senado:
            senado_link = re.search('p_cod_mate=([0-9]*)', senado['href']).group(1)
        else:
            senado_link = None
        return r.index(camara_link, senado_link, lei)

class dashboard:
    def GET(self, casa, project_id):
        if project_id:
        #try:            
            if casa == 'camara':
                projeto = Project(project_id, 'CAMARA')
                projeto.getTramitacaoCamara()
            elif casa == 'senado':
                projeto = Project(project_id, 'SENADO')
                projeto.getTramitacaoSenado()
            dashboard = Dashboard(projeto)
            msg = dashboard.form('teste')
            html = dashboard.render(msg)       
            return dashboard.render(msg)
        #except:
        #    return 'Wrong ID:' + project_id
        else:
            return 'No ID.'


    def POST(self, project):
        i = web.input()
        mail_from = "contato@ondeestaalei.com.br"
        real_comission_members = i['comission_members'].split(";")
        real_comission_members.pop()
        comission_members = ['pedro@esfera.mobi']
        mail_subject = "Onde esta a Lei numero X?"
        mail_message = i['msg'].replace("#nome#",i['name'])

        #sendmail not working
        web.config.smtp_server = 'smtp.gmail.com'
        web.config.smtp_port = 587
        web.config.smtp_username = 'averon@gmail.com'
        web.config.smtp_password = 'klakinun'
        web.config.smtp_starttls = True
 
        web.sendmail(mail_from, comission_members, mail_subject, mail_message)

        return "Mensagem:" + mail_message + str(real_comission_members)

class history:
    def GET(self, casa, project_id):
        if project_id:
        #try:            
            if casa == 'camara':
                projeto = Project(project_id, 'CAMARA')
                projeto.getTramitacaoCamara()
            elif casa == 'senado':
                projeto = Project(project_id, 'SENADO')
                projeto.getTramitacaoSenado()
            history = History(projeto)
            html = history.render()       
            return history.render()
        #except:
        #    return 'Wrong ID:' + project_id 
        else:
            return 'No ID.'


class widget:
    def GET(self, project_id):
        filename = settings['html_path'] + project_id + '.html'
        if os.path.exists(filename):
            last_mod = time.gmtime(os.path.getmtime(filename))
            last_mod = datetime.datetime.fromtimestamp(time.mktime(last_mod))
            t = datetime.datetime.today() - last_mod
            if  t.days > 0:
                print 'Loading project ' + project_id + '...'
                dashboard = Dashboard(project_id)
                html = dashboard.render()
                dashboard.writefile(html)
                web.seeother(filename)
            else:
                web.seeother(filename)
        else:
            print 'Loading project ' + project_id + '...'
            dashboard = Dashboard(project_id)
            html = dashboard.render()
            dashboard.writefile(html)
            web.seeother(filename)

if __name__ == "__main__": app.run()

app = web.application(urls, globals(), autoreload=False)
application = app.wsgifunc()

