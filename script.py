#!/usr/bin/python
# -*- coding: utf-8 -*-

import urllib
#a ultima versao quebra no site do senado. usando 3.0.8
import BeautifulSoup
import re

def getIds(tipo, numero, ano):
    base_url = "http://www.lexml.gov.br/urn/urn:lex:br:"
    lei = ''

    if tipo == 'plc':
        lei += 'camara.deputados:projeto.lei;pl:'+ str(ano) + ';' + str(numero)
    elif tipo == 'pls':
        lei += 'senado.federal:projeto.lei;pls:'+ str(ano) + ';' + str(numero)
    elif tipo == 'pdc':
        lei += 'camara.deputados:projeto.decreto.legislativo;pdc:'+ str(ano) + ';' + str(numero)
    elif tipo == 'plp':
        lei += 'camara.deputados:projeto.lei.complementar;plp:'+ str(ano) + ';' + str(numero)
    elif tipo == 'pec':
        lei += 'camara.deputados:proposta.emenda.constitucional;pec:'+ str(ano) + ';' + str(numero)
    else:
        print 'Categoria Invalida.'
    
    html = urllib.urlopen(base_url+lei)          
    soup = BeautifulSoup.BeautifulSoup(html)

    camara = soup.find('a', { 'href' : re.compile('www.camara.gov.br/sileg/Prop_Detalhe.asp\?id=[0-9]*') })

    senado = soup.find('a', { 'href' : re.compile('www.senado.gov.br/sf/atividade/Materia/detalhes.asp\?p_cod_mate=[0-9]*') })

    camara = re.search('id=([0-9]*)', camara['href']).group(1)
    senado = re.search('p_cod_mate=([0-9]*)', senado['href']).group(1)

    
